# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/16

"""
import getopt
import json
import logging
import sys

from confluence import Confluence
from ones import Ones
from util import load_json_data, save_json_data

logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

spaces_data_file = './spaces.json'


def load_config(cfg_file):
    with open(cfg_file, 'r') as f:
        return json.loads(f.read())


def load_all_space_keys_from_api(confluence_client):
    logging.info('load all space keys from api')
    return confluence_client.list_all_space_keys()


def load_spaces(confluence_client):
    spaces_data = load_json_data(spaces_data_file)
    keys = load_all_space_keys_from_api(confluence_client)
    if not spaces_data:
        return {key: {'export_file': '', 'import_done': False} for key in keys}
    for key in keys:
        if spaces_data.get(key) is None:
            spaces_data[key] = {'export_file': '', 'import_done': False}
    return spaces_data


def dump_spaces_data(confluence_client):
    spaces_data = load_spaces(confluence_client)
    save_json_data(spaces_data, spaces_data_file)


def migrate_spaces(confluence_client, ones_client, spaces):
    try:
        for space_key, space_status in spaces.items():
            space_file = space_status['export_file']
            if not space_file:
                space_file = confluence_client.export_space_html(space_key)
                space_status['export_file'] = space_file
            else:
                logging.info(f'space already exported: {space_key}')

            if not space_status['import_done']:
                import_done = ones_client.import_confluence(space_file)
                space_status['import_done'] = import_done
                if import_done:
                    logging.info(f'space import success: {space_key}')
                else:
                    logging.error(f'space import failed: {space_key}')
            else:
                logging.error(f'space already imported: {space_key}')
    except Exception as e:
        logging.error(e)
    finally:
        save_json_data(spaces, spaces_data_file)


def export_spaces(confluence_client, spaces):
    try:
        for space_key, space_status in spaces.items():
            space_file = space_status['export_file']
            if not space_file:
                space_file = confluence_client.export_space_html(space_key)
                spaces[space_key]['export_file'] = space_file
            else:
                logging.info(f'space already exported: {space_key}')
    except Exception as e:
        logging.error(e)
    finally:
        save_json_data(spaces, spaces_data_file)


def import_spaces(ones_client):
    spaces = load_json_data(spaces_data_file)
    try:
        for space_key, space_status in spaces.items():
            import_done = space_status['import_done']
            space_file = space_status['export_file']
            if import_done:
                logging.info(f'space file already imported: {space_file}')
                continue
            space_status['import_done'] = ones_client.import_confluence(space_file)
    except Exception as e:
        logging.error(e)
    finally:
        save_json_data(spaces, spaces_data_file)


def main(exe, argv):
    config_file = ''
    only_export = False
    only_import = False
    spaces_dir = ''
    is_dump_spaces_data = False

    help_content = f"""conba usage:
1. export and import spaces: {exe} -c <config_file> [ -d <spaces_dir> ]
2. only export spaces: {exe} -c <config_file> -e [ -d <spaces_dir> ]
3. only import spaces: {exe} -c <config_file> -i [ -d <spaces_dir> ]
4. dump spaces data: {exe} -c <config_file> -l"""

    try:
        opts, args = getopt.getopt(argv, 'heic:d:l')
        for opt, arg in opts:
            if opt == '-h':
                print(help_content)
                sys.exit(0)
            elif opt == '-c':
                config_file = arg
            elif opt == '-e':
                only_export = True
            elif opt == '-i':
                only_import = True
            elif opt == '-d':
                spaces_dir = arg
            elif opt == '-l':
                is_dump_spaces_data = True
            else:
                print(f'unknown option: {opt}')
                print(help_content)
                sys.exit(1)
    except getopt.GetoptError as e:
        logging.error(f'get options failed: {e}')
        sys.exit(1)

    if only_export and only_import:
        logging.error('cannot set -e and -i at the same time')
        sys.exit(1)

    do_migrate = not only_export and not only_import and not is_dump_spaces_data

    if spaces_dir == '':
        spaces_dir = 'spaces'

    if config_file == '':
        print('config file is necessary')
        print(help_content)
        sys.exit(1)

    config = load_config(config_file)

    ones_client = None
    if only_import or do_migrate:
        ones_client = Ones(config['ones_base_url'], config['ones_team_uuid'], config['ones_login_email'],
                           config['ones_login_password'], config['ones_max_import_waiting_time'])

    confluence_client = None
    if only_export or do_migrate or is_dump_spaces_data:
        confluence_client = Confluence(config['confluence_base_url'], config['confluence_username'],
                                       config['confluence_password'], config['ones_import_file_size_limit'],
                                       spaces_dir)

    if is_dump_spaces_data:
        dump_spaces_data(confluence_client)
        sys.exit(0)

    if only_import:
        import_spaces(ones_client)
        sys.exit(0)

    spaces = load_spaces(confluence_client)
    logging.info(f'all spaces count: {len(spaces)}')
    if only_export:
        export_spaces(confluence_client, spaces)
    else:
        migrate_spaces(confluence_client, ones_client, spaces)
