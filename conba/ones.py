# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/16

"""
import json
import logging
import os
import time
import zipfile

import requests

from base import BaseClient


class Ones(BaseClient):
    def __init__(self, base_url, team_uuid, email, password, max_import_waiting_time):
        super().__init__()
        self.base_url = base_url[:-1] if base_url.endswith('/') else base_url

        self.team_uuid = team_uuid
        self.email = email
        self.password = password
        self.max_import_waiting_time = max_import_waiting_time if isinstance(max_import_waiting_time,
                                                                             int) and max_import_waiting_time > 0 else 60

        self.session = None
        self._login()

    def upload_confluence_file(self, filepath):
        endpoint = f'/project/team/{self.team_uuid}/res/attachments/upload'
        filename = os.path.basename(filepath)
        payload = {
            'name': filename,
            'ref_type': 'confluence',
            'type': 'confluence_backup'
        }
        with self.post(endpoint, json=payload) as r:
            if r.status_code == requests.codes.ok:
                data = r.json()
                upload_url = data['upload_url']
                token = data['token']
                resource_uuid = data['resource_uuid']
                if self.upload_file(filepath, upload_url, token):
                    return resource_uuid

            self.http_error(r)
            return None

    def upload_file(self, filepath, upload_url, token):
        payload = {
            'token': token
        }
        files = {
            'file': open(filepath, 'rb')
        }
        with self.session.post(upload_url, data=payload, files=files) as r:
            return r.status_code == requests.codes.ok

    @staticmethod
    def is_valid_space_file(space_file):
        return zipfile.is_zipfile(space_file)

    def import_confluence(self, space_file):
        if not space_file:
            logging.error('space file cannot be empty')
            return False

        if not self.is_valid_space_file(space_file):
            logging.error(f'invalid space file: {space_file}')
            return False

        logging.info(f'start import confluence: {space_file}')
        resource_uuid = self.upload_confluence_file(space_file)
        if not resource_uuid:
            return

        endpoint = f'/wiki/team/{self.team_uuid}/confluence/import'
        payload = {
            'resource_uuids': [resource_uuid],
            'type': 'space'
        }
        with self.post(endpoint, json=payload) as r:
            if r.status_code == requests.codes.ok:
                return self.check_import_confluence_status(resource_uuid)
            else:
                self.http_error(r)
                return False

    def list_queues(self):
        endpoint = f'/project/team/{self.team_uuid}/queues/list'
        with self.get(endpoint) as r:
            if r.status_code == requests.codes.ok:
                return r.json()['batch_tasks']
            else:
                self.http_error(r)
                return None

    def check_import_confluence_status(self, resource_uuid):
        start_time = time.time()
        while True:
            queues = self.list_queues()
            if queues is None:
                return False

            for queue in queues:
                extra = json.loads(queue['extra'])
                if queue['job_type'] == 'import_confluence_space' and extra['resource_uuid'] == resource_uuid:
                    logging.info(
                        f'successful count: {queue["successful_count"]}, unprocessed count: {queue["unprocessed_count"]}, unsuccessful count: {queue["unsuccessful_count"]}')
                    if queue['job_status'] == 'done':
                        if extra['space_uuid'] == '' and extra['page_uuid'] == '':
                            logging.error('import confluence failed')
                            return False
                        else:
                            logging.info('import confluence success')
                            return True
            if time.time() - start_time > self.max_import_waiting_time:
                logging.error(f'check import confluence status timeout')
                return False
            time.sleep(5)

    def post(self, endpoint, data=None, json=None, **kwargs):
        url = self.url(endpoint)
        return self.session.post(url, data, json, **kwargs)

    def get(self, endpoint, **kwargs):
        url = self.url(endpoint)
        return self.session.get(url, **kwargs)

    def _login(self):
        logging.info('trying login ONES...')
        self.session = requests.Session()
        endpoint = '/project/auth/login'
        url = self.url(endpoint)
        payload = {
            'email': self.email,
            'password': self.password
        }
        with self.session.post(url, json=payload) as r:
            if r.status_code == requests.codes.ok:
                user = r.json()['user']
                self.session.headers = {
                    'Ones-User-ID': user['uuid'],
                    'Ones-Auth-Token': user['token']
                }
                logging.info('ONES login success')
            else:
                self.http_error(r)
