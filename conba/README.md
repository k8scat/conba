# Conba

`Confluence` 批量导出导入工具 for `ONES`

## 支持

- [x] 批量导出
- [x] 批量导入
- [x] 批量导出和导入
- [x] 导出 `spaces` 数据文件

## 系统版本适配

### Confluence

- [x] `5.x`
- [x] `6.x`
- [x] `7.x`
  
### ONES

- [x] 20210104

## 运行

### Dev

环境要求: `python 3`

```bash
make
```

### Prod

> Unix

```bash
# build
make build

# run
./conba -c config.json
```

> Windows

Refer to [Makefile](./Makefile)

```bash
./conba.exe -c config.json
```

## 配置文件说明

- 如何确定 `ones_base_url` ?

打开前端控制台 (刷新页面), 选一个 `XHR` 类型的 `Network` 请求, 获取到请求的 `Request URL`,
例如: `https://ones.ai/project/api/project/auth/token_info`,
`ones_base_url` 取 `/project/auth/token_info` 前面的部分,
即 `https://ones.ai/project/api`.

## 支持中断

当 `批量导出导入` 执行失败时,
会将数据记录在 `spaces.json` 文件中,
并在重新执行迁移时读取文件, 并判断 `space` 是否需要重新进行 `导出` 或 `导入`.

记录的数据格式如下:

```
# spaces.json
# 该文件会记录所有需要 迁移 或 导出 的 space 信息
# export_file 在 导出成功 后, 被设置为 spaces_dir/space_zip_filename, 默认为空, 表示没有导出
# import_done 表示 是否导入完成
{
    "space_key_1": {
      "export_file": "",
      "import_done": false
    },
    "space_key_2": {
      "export_file": "spaces/space_key-uuid.zip",
      "import_done": true
    }
}
```

注意: `导出` 和 `导入` 是整个执行过程中最小的单元, 不可进一步切分.

## 支持自定义空间

配置文件同 `spaces.json`.

## 获取 Space Keys

使用 `make dump_spaces` 或 `./conba -c config.json -l` 生成 `spaces.json` 文件, 其中就包含所有的 `space keys`.

## 跨平台构建

[release_conba.yaml](../.github/workflows/release_conba.yaml)

## 导入数量限制

`ONES` 系统在没有授权的情况下, `Confluence` 导入的数量会受到限制.
