# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/16

"""
import logging
import os
import uuid
import zipfile
from urllib.parse import urlencode

import requests
from bs4 import BeautifulSoup

from base import BaseClient


class Confluence(BaseClient):
    def __init__(self, base_url, username, password, file_size_limit, export_dir):
        super().__init__()
        self.base_url = base_url[:-1] if base_url.endswith('/') else base_url
        self.username = username
        self.password = password
        self.file_size_limit = file_size_limit

        self.export_dir = export_dir
        if not os.path.isdir(self.export_dir):
            os.makedirs(self.export_dir)

        self.headers = {
            'Accept-Language': 'en'
        }

        self.session = None
        self.atl_token = ''
        self._login()

    def list_space_keys(self, query, page_size=24, start_index=0):
        headers = {
            'Accept': 'application/json'
        }
        params = {
            'query': query,
            'pageSize': page_size,
            'startIndex': start_index
        }
        endpoint = '/rest/spacedirectory/1/search'
        with self.get(endpoint, params=params, headers=headers) as r:
            if r.status_code == requests.codes.ok:
                data = r.json()
                keys = [space['key'] for space in data['spaces']]
                return dict(keys=keys, total=data['totalSize'])
            else:
                logging.error(f'list spaces failed: {r.status_code}, {r.text}')
                return None

    def list_all_space_keys(self):
        page_size = 100
        start_index = 0
        all_keys = []
        while True:
            res = self.list_space_keys('', page_size, start_index)
            if res is None:
                return
            all_keys.extend(res['keys'])
            if len(all_keys) == res['total']:
                break
            start_index += len(res['keys'])
        return all_keys

    def export_space_html(self, key):
        logging.info(f'start export space: {key}')
        endpoint = '/spaces/doexportspace.action'
        params = {
            'key': key
        }
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        space = self._get_export_space(key)
        export_token = self._get_export_token(key)
        payload = {
            'atl_token': export_token,
            'synchronous': 'true',
            'exportType': 'TYPE_HTML',
            'includeComments': 'true',
            'contentToBeExported': space
        }
        payload = urlencode(payload)
        with self.post(endpoint, data=payload, headers=headers,
                       params=params, stream=True) as r:
            if r.status_code == requests.codes.ok:
                space_size = r.headers.get("Content-Length")
                if space_size:
                    space_size = int(space_size)
                else:
                    logging.error('get space size failed')
                    return None

                if space_size > self.file_size_limit:
                    logging.error(f'space size exceeds the limit ({self.file_size_limit}): {space_size}')
                    return None

                logging.info(f'space size: {r.headers.get("Content-Length")} bytes')
                filename = os.path.join(self.export_dir, f'{key}_{str(uuid.uuid1())}.zip')
                with open(filename, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)

                if space_size != os.path.getsize(filename) or not zipfile.is_zipfile(filename):
                    logging.error(f'invalid exported space file: {filename}')
                    return None

                logging.info(f'space export success: {key}')
                return filename

            self.http_error(r)
            return None

    def create_random_space(self):
        endpoint = '/rest/create-dialog/1.0/space-blueprint/create-space'
        unq_name = str(uuid.uuid1()).replace('-', '')
        space_blueprint_id = self._get_blank_space_blueprint()
        payload = {
            'name': unq_name,
            'spaceKey': unq_name,
            'spaceBlueprintId': space_blueprint_id
        }
        with self.post(endpoint, json=payload) as r:
            if r.status_code == requests.codes.ok:
                data = r.json()
                return data["url"]
            else:
                self.http_error(r)
                return None

    def _get_export_space(self, key):
        endpoint = '/spaces/exportspacetree.action'
        params = {
            'key': key,
        }
        with self.get(endpoint, params=params) as r:
            if r.status_code == requests.codes.ok:
                try:
                    soup = BeautifulSoup(r.text, 'lxml')
                    # 只获取父页面的值即可
                    space = soup.select('li.content-tree-node:nth-of-type(1) input.exportContentTreeCheckbox')[0][
                        'value']
                    return space
                except Exception as e:
                    logging.error(f'get export space value failed: {e}')
                    return None
            else:
                self.http_error(r)
                return None

    def _get_blank_space_blueprint(self):
        endpoint = '/rest/create-dialog/1.0/space-blueprint/dialog/web-items'
        blank_space_name = 'Blank space'
        with self.get(endpoint) as r:
            if r.status_code == requests.codes.ok:
                spaces = r.json()
                for space in spaces:
                    if space['name'] == blank_space_name:
                        return space['contentBlueprintId']

                logging.info('blank space blueprint not found')
                return None
            else:
                self.http_error(r)
                return None

    def _get_atl_token(self):
        with self.get('') as r:
            if r.status_code == requests.codes.ok:
                try:
                    soup = BeautifulSoup(r.text, 'lxml')
                    atl_token = soup.select('meta#atlassian-token')[0]['content']
                    return atl_token
                except Exception as e:
                    logging.error(f'get atl_token failed: {e}')
                    return None
            else:
                self.http_error(r)
                return None

    def _login(self):
        logging.info('trying login Confluence...')
        login_endpoint = '/dologin.action'
        url = self.url(login_endpoint)
        data = {
            'os_username': self.username,
            'os_password': self.password,
            'os_destination': ''
        }
        self.session = requests.Session()
        with self.session.post(url, data) as r:
            if r.status_code == 200:
                self.atl_token = self._get_atl_token()
                if self.atl_token is not None:
                    logging.info('Confluence login success')
                    return True

            self.http_error(r)
            return False

    def post(self, endpoint, data=None, json=None, **kwargs):
        self.keep_session()
        kwargs = self._set_default_headers(**kwargs)
        url = self.url(endpoint)
        return self.session.post(url, data, json, **kwargs)

    def get(self, endpoint, **kwargs):
        self.keep_session()
        kwargs = self._set_default_headers(**kwargs)
        url = self.url(endpoint)
        return self.session.get(url, **kwargs)

    def _get_export_token(self, key):
        endpoint = '/spaces/exportspacehtml.action'
        params = {
            'key': key
        }
        with self.get(endpoint, params=params) as r:
            if r.status_code == requests.codes.ok:
                try:
                    soup = BeautifulSoup(r.text, 'lxml')
                    token = soup.select('input[name="atl_token"]')[0]['value']
                    return token
                except Exception as e:
                    logging.error(f'get export token failed: {e}')
                    return None
            self.http_error(r)
            return None

    def _set_default_headers(self, **kwargs):
        if kwargs.get('headers') is None:
            kwargs.setdefault('headers', self.headers)
        else:
            for k, v in self.headers.items():
                if not kwargs['headers'].get(k):
                    kwargs['headers'][k] = v
        return kwargs

    def keep_session(self):
        if self.session is None or not self.is_valid_session():
            if not self._login():
                raise Exception('confluence login failed')
            logging.info('confluence login success')

    def is_valid_session(self):
        if self.session is None:
            return False

        endpoint = '/rest/mywork/latest/status/notification/count'
        url = self.url(endpoint)
        with self.session.get(url) as r:
            return r.status_code == requests.codes.ok

    def make_export_dir(self):
        if self.export_dir == '':
            return True
        if not os.path.isdir(self.export_dir):
            os.makedirs(self.export_dir)
