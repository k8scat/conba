# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/19

"""
import sys

from conba import main

if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])
