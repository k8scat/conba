# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/16

"""
import logging

import config
from confluence import Confluence

if __name__ == '__main__':
    con = Confluence(config.CONFLUENCE_BASE_URL, config.CONFLUENCE_USERNAME, config.CONFLUENCE_PASSWORD)
    for i in range(2000):
        space_url = con.create_random_space()
        if space_url is None:
            exit(1)
        logging.info(f'space url: {space_url}')
