# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/31

"""
import json
import logging
import os


def load_json_data(filepath):
    if os.path.isfile(filepath):
        try:
            with open(filepath, 'r') as f:
                return json.loads(f.read())
        except Exception as e:
            logging.error(f'load json data failed: {e}')
    return {}


def save_json_data(data, filepath):
    try:
        with open(filepath, 'w') as f:
            f.write(json.dumps(data))
        logging.info(f'save data success: {filepath}')
    except Exception as e:
        logging.error(f'write file failed: {e}')
