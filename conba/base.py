# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/12/17

"""
import logging


class BaseClient:
    def __init__(self):
        self.base_url = ''

    def url(self, endpoint):
        if len(endpoint) == 0:
            return self.base_url

        if endpoint[0] != '/':
            endpoint = f'/{endpoint}'
        return f'{self.base_url}{endpoint}'

    @staticmethod
    def http_error(resp):
        logging.error(f'request {resp.url} {resp.status_code}: {resp.text}')
